public class Princpal{
    public static void main(String[] args){
        Princpal.engine((int x,int y) -> x + y);
        Princpal.engine((long x,long y) -> x * y);
        Princpal.engine((int x,int y) -> x / y);
        Princpal.engine((long x,long y) -> x - y);
        Princpal.engine((int x,int y) -> x % y);
    }

    //Sobrecarga de Métodos
    private static void engine(Calculadoraint cal){
        int x=2, y=4;
        int resultado = cal.calculate(x,y);
        System.out.println("Resultado = " + resultado);
    }

    private static void engine(CalculadoraLong cal){
        long x=2, y=4;
        long resultado = cal.calculate(x,y);
        System.out.println("Resultado = " + resultado);
    }

}